//
//  Note.swift
//  NoteApp
//
//  Created by BTB_015 on 12/10/20.
//

import Foundation

struct NoteItem {
    var title: String
    var desc: String
}
