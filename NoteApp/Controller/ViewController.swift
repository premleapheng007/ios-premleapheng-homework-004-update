//
//  ViewController.swift
//  NoteApp
//
//  Created by BTB_015 on 12/10/20.
//

import UIKit

protocol EditDelegate {
    func addDelegae(id: Int, title: String, desc: String)
}

class ViewController: UIViewController, AddDelegate{
    
  
    @IBOutlet weak var collectionView: UICollectionView!
    
    //Emtity Object
    var note: [Note] = []
    
    //Create context for Coredata
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //Edit Delegate Object
    var editDelegat: EditDelegate?
    
    var getTitle = ""
    var getDesc = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(UINib(nibName: "NoteItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "itemCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        collectionView.reloadData()
        fetchData()

    }
    
    func addDelegae(title: String, desc: String) {
        if title == "" && desc == "" {
            print("object nil")
        }else{
            postData(title: title, desc: desc)
        }
    }
    
    func postData(title: String, desc: String)  {
        //Create for accessing atribute at table CoreData
        let contact = Note(context: self.context)
        contact.noteTitle = title
        contact.noteDesc = desc
        try! context.save()
    }
    
    func fetchData() {
        self.note = try! context.fetch(Note.fetchRequest())
    }
 
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return note.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemCell", for: indexPath) as! NoteItemCollectionViewCell
        
        cell.noteTitel.text = note[indexPath.row].noteTitle
        cell.noteDesc.text = note[indexPath.row].noteDesc
        
        let longPress = UILongPressGestureRecognizer()
        self.view.addGestureRecognizer(longPress)
        longPress.addTarget(self, action: #selector(handleLongPress))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        let SVC = self.storyboard?.instantiateViewController(withIdentifier: "editView") as! EditViewController
        self.editDelegat = SVC
        editDelegat?.addDelegae(id: indexPath.row, title:note[indexPath.row].noteTitle!, desc:note[indexPath.row].noteDesc!)
        self.navigationController?.pushViewController(SVC, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 170, height: 170)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
      return 24
   }
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 25, left: 25, bottom: 0, right: 25)
   }

    @objc func handleLongPress(sender: UILongPressGestureRecognizer){

        let alert = UIAlertController(title: "Delte Item!", message: "Do you want to delete this item?", preferredStyle: .alert)
    
        if sender.state == UIGestureRecognizer.State.began {
            let touchPoint = sender.location(in: collectionView)
            if let index = collectionView.indexPathForItem(at: touchPoint) {
                alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: {_ in
                    self.context.delete(self.note[index.row])
                    self.fetchData()
                    self.collectionView.reloadData()
                }))
            }
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
}

