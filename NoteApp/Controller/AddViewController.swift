//
//  AddViewController.swift
//  NoteApp
//
//  Created by BTB_015 on 12/10/20.
//

import UIKit

protocol AddDelegate {
    func addDelegae(title: String, desc: String)
}

class AddViewController: UIViewController {

    @IBOutlet weak var noteTitle: UITextField!
    @IBOutlet weak var noteDesc: UITextView!
    
    var addDelegaeObj: AddDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noteTitle.borderStyle = .none

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "VC") as! ViewController
        self.addDelegaeObj = VC
        addDelegaeObj?.addDelegae(title: noteTitle.text!, desc: noteDesc.text!)
    }
}
