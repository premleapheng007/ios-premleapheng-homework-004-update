//
//  EditViewController.swift
//  NoteApp
//
//  Created by BTB_015 on 12/10/20.
//

import UIKit

class EditViewController: UIViewController, EditDelegate {

    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var note: [Note] = []

    @IBOutlet weak var noteTitle: UITextField!
    @IBOutlet weak var noteDesc: UITextView!
    
    var getTitle = ""
    var getDesc = ""
    var getId = 0
    
    override func viewDidLoad() {

        super.viewDidLoad()
        noteTitle.borderStyle = .none
        noteTitle.text = getTitle
        noteDesc.text = getDesc
        fetchData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
       
        update(id: getId, title: noteTitle.text! , desc: noteDesc.text!)
        fetchData()
        
    }
    
    func addDelegae(id: Int, title: String, desc: String) {
        getTitle = title
        getDesc = desc
        getId = id
    }
    
    func update(id: Int, title: String, desc: String) {
        print(id)
        let updateNote = note[id]
        updateNote.noteTitle  = title
        updateNote.noteDesc = desc
        try! context.save()
    }
    
    func fetchData() {
        self.note = try! context.fetch(Note.fetchRequest())
    }
}
